package pl.edu.agh.msroka.dto;

import org.springframework.stereotype.Component;
import pl.edu.agh.msroka.entities.*;

import java.util.HashSet;
import java.util.Set;

@Component
public class DTOConverter {
    public InvoiceDTO toInvoiceDTO(Invoice invoice) {
        InvoiceDTO invoiceDTO = new InvoiceDTO();
        invoiceDTO.setId(invoice.getId());
        invoiceDTO.setNumber(invoice.getNumber());
        invoiceDTO.setIssueDate(invoice.getIssueDate());
        invoiceDTO.setIssueCity(invoice.getIssueCity());
        invoiceDTO.setSaleDate(invoice.getSaleDate());
        invoiceDTO.setPayoffDate(invoice.getPayoffDate());
        invoiceDTO.setAmount(invoice.getAmount());
        invoiceDTO.setPaidAmount(invoice.getPaidAmount());
        invoiceDTO.setPaymentForm(invoice.getPaymentForm());

        Set<ItemDTO> itemDTOs = new HashSet<>();
        for(Item item : invoice.getItems()) {
            itemDTOs.add(toItemDTO(item));
        }
        invoiceDTO.setItems(itemDTOs);
        Set<Payment> payments = invoice.getPayments();
        if(payments == null) {
            Set<PaymentDTO> paymentDTOs = new HashSet<>();
            invoiceDTO.setPayments(paymentDTOs);
        } else {
            Set<PaymentDTO> paymentDTOs = new HashSet<>();
            for (Payment payment : payments) {
                paymentDTOs.add(toPaymentDTO(payment));
            }
            invoiceDTO.setPayments(paymentDTOs);
        }


        invoiceDTO.setClient(toClientDTO(invoice.getClient()));
        invoiceDTO.setSeller(toSellerDTO(invoice.getSeller()));
        return invoiceDTO;
    }
    public Invoice fromInvoiceDTO(InvoiceDTO invoiceDTO) {
        Invoice invoice = new Invoice();
        invoice.setId(invoiceDTO.getId());
        invoice.setNumber(invoiceDTO.getNumber());
        invoice.setIssueDate(invoiceDTO.getIssueDate());
        invoice.setIssueCity(invoiceDTO.getIssueCity());
        invoice.setSaleDate(invoiceDTO.getSaleDate());
        invoice.setPayoffDate(invoiceDTO.getPayoffDate());
        invoice.setPaymentForm(invoiceDTO.getPaymentForm());

        Set<Item> items = new HashSet<>();
        for (ItemDTO itemDTO : invoiceDTO.getItems()) {
            Item item = fromItemDTO(itemDTO);
            item.setInvoice(invoice);
            items.add(item);
        }
        invoice.setItems(items);

        Set<PaymentDTO> paymentsDTOs = invoiceDTO.getPayments();
        if(paymentsDTOs == null) {
            invoice.setPayments(null);
        } else {
            Set<Payment> payments = new HashSet<>();
            for (PaymentDTO paymentDTO : paymentsDTOs) {
                Payment payment = fromPaymentDTO(paymentDTO);
                payment.setInvoice(invoice);
                payments.add(payment);
            }
            invoice.setPayments(payments);
        }
        
        invoice.setClient(this.fromClientDTO(invoiceDTO.getClient()));
        invoice.setSeller(this.fromSellerDTO(invoiceDTO.getSeller()));
        return invoice;
    }

    public ItemDTO toItemDTO(Item item) {
        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(item.getId());
        itemDTO.setName(item.getName());
        itemDTO.setPriceNet(item.getPriceNet());
        itemDTO.setQuantity(item.getQuantity());
        itemDTO.setUnit(item.getUnit());
        itemDTO.setVat(item.getVat());
        return itemDTO;
    }
    public Item fromItemDTO(ItemDTO itemDTO) {
        Item item = new Item();
        item.setId(itemDTO.getId());
        item.setName(itemDTO.getName());
        item.setPriceNet(itemDTO.getPriceNet());
        item.setQuantity(itemDTO.getQuantity());
        item.setUnit(itemDTO.getUnit());
        item.setVat(itemDTO.getVat());
        return item;
    }

    public ClientDTO toClientDTO(Client client) {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setId(client.getId());
        clientDTO.setName(client.getName());
        clientDTO.setNip(client.getNip());
        clientDTO.setStreet(client.getStreet());
        clientDTO.setPostalCode(client.getPostalCode());
        clientDTO.setCity(client.getCity());
        clientDTO.setPhone(client.getPhone());
        clientDTO.setEmail(client.getEmail());
        clientDTO.setWebsite(client.getWebsite());
        clientDTO.setDescription(client.getDescription());
        return clientDTO;
    }

    public SellerDTO toSellerDTO(Seller seller) {
        SellerDTO sellerDTO = new SellerDTO();
        sellerDTO.setId(seller.getId());
        sellerDTO.setName(seller.getName());
        sellerDTO.setNip(seller.getNip());
        sellerDTO.setStreet(seller.getStreet());
        sellerDTO.setPostalCode(seller.getPostalCode());
        sellerDTO.setCity(seller.getCity());
        sellerDTO.setPhone(seller.getPhone());
        sellerDTO.setEmail(seller.getEmail());
        sellerDTO.setWebsite(seller.getWebsite());
        sellerDTO.setDescription(seller.getDescription());
        return sellerDTO;
    }
    
    public Client fromClientDTO(ClientDTO clientDTO) {
        Client client = new Client();
        client.setId(clientDTO.getId());
        client.setName(clientDTO.getName());
        client.setCity(clientDTO.getCity());
        client.setDescription(clientDTO.getDescription());
        client.setEmail(clientDTO.getEmail());
        client.setNip(clientDTO.getNip());
        client.setPhone(clientDTO.getPhone());
        client.setPostalCode(clientDTO.getPostalCode());
        client.setStreet(clientDTO.getStreet());
        client.setWebsite(clientDTO.getStreet());
        return client;
    }

    public Seller fromSellerDTO(SellerDTO sellerDTO) {
        Seller seller = new Seller();
        seller.setId(sellerDTO.getId());
        seller.setName(sellerDTO.getName());
        seller.setCity(sellerDTO.getCity());
        seller.setDescription(sellerDTO.getDescription());
        seller.setEmail(sellerDTO.getEmail());
        seller.setNip(sellerDTO.getNip());
        seller.setPhone(sellerDTO.getPhone());
        seller.setPostalCode(sellerDTO.getPostalCode());
        seller.setStreet(sellerDTO.getStreet());
        seller.setWebsite(sellerDTO.getStreet());
        return seller;
    }

    public PaymentDTO toPaymentDTO(Payment payment) {
        PaymentDTO paymentDTO = new PaymentDTO();
        paymentDTO.setId(payment.getId());
        paymentDTO.setAmount(payment.getAmount());
        paymentDTO.setDescription(payment.getDescription());
        return paymentDTO;
    }
    public Payment fromPaymentDTO(PaymentDTO paymentDTO) {
        Payment payment = new Payment();
        payment.setId(paymentDTO.getId());
        payment.setAmount(paymentDTO.getAmount());
        payment.setDescription(paymentDTO.getDescription());
        return payment;
    }

    public CompanyDTO toCompanyDTO(Company company) {
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setName(company.getName());
        companyDTO.setNip(company.getNip());
        companyDTO.setAddress(company.getAddress());
        companyDTO.setCity(company.getCity());
        companyDTO.setPostalCode(company.getPostalCode());
        companyDTO.setPhone(company.getPhone());
        companyDTO.setEmail(company.getEmail());
        return companyDTO;
    }
    public Company fromCompanyDTO(CompanyDTO companyDTO, Company company) {
        company = company == null ? new Company() : company;
        company.setName(companyDTO.getName());
        company.setNip(companyDTO.getNip());
        company.setAddress(companyDTO.getAddress());
        company.setCity(companyDTO.getCity());
        company.setPostalCode(companyDTO.getPostalCode());
        company.setPhone(companyDTO.getPhone());
        company.setEmail(companyDTO.getEmail());
        return company;
    }
    public Company fromCompanyDTO(CompanyDTO companyDTO) {
        return fromCompanyDTO(companyDTO, null);
    }

    public ReportInvoiceDTO toReportInvoiceDTO(Invoice invoice) {
        ReportInvoiceDTO reportInvoiceDTO = new ReportInvoiceDTO();
        reportInvoiceDTO.setAmount(invoice.getAmount());
        reportInvoiceDTO.setNumber(invoice.getNumber());
        reportInvoiceDTO.setPaymentForm(invoice.getPaymentForm());
        reportInvoiceDTO.setPaidAmount(invoice.getPaidAmount());
        reportInvoiceDTO.setSellerName(invoice.getSeller().getName());
        reportInvoiceDTO.setSellerNip(invoice.getSeller().getNip());
        return reportInvoiceDTO;
    }
}
