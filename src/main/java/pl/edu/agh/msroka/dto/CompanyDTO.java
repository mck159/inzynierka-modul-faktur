package pl.edu.agh.msroka.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class CompanyDTO {
    @NotEmpty
    @Length(min=5, max=50)
    String name;
    @NotEmpty
    @Pattern(regexp = "^[0-9]{10}$")
    String nip;
    @NotEmpty
    @Pattern(regexp = "^[0-9]{2}-[0-9]{3}$")
    String postalCode;
    @NotEmpty
    @Length(min=5, max=100)
    String city;
    @NotEmpty
    @Length(min=5, max=200)
    String address;
    @NotEmpty
    @Pattern(regexp = "^+?[0-9]{9,11}$")
    String phone;
    @NotEmpty
    @Email
    String email;

    public CompanyDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
