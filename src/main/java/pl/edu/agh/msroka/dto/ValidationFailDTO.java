package pl.edu.agh.msroka.dto;

import org.springframework.validation.FieldError;

import java.util.List;

/**
 * Created by maciek on 12.11.15.
 */
public class ValidationFailDTO {
    public ValidationFailDTO() {
    }
    String field;
    String message;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
