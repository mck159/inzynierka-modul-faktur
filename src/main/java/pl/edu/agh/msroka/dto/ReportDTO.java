package pl.edu.agh.msroka.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.NotEmpty;
import pl.edu.agh.msroka.entities.ReportType;
import pl.edu.agh.msroka.entities.utils.PaymentForm;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public class ReportDTO {
    int id;

    @NotEmpty
    @Pattern(regexp = "^F[A-Za-z0-9_\\-]+$")
    ReportType type;

    Map<String, Object> params;

    public ReportDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ReportType getType() {
        return type;
    }

    public void setType(ReportType type) {
        this.type = type;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
}
