package pl.edu.agh.msroka.exceptions;

/**
 * Created by maciek on 10.11.15.
 */
public class AuthException extends RuntimeException {
    public AuthException(String s) {
        super(s);
    }
}
