package pl.edu.agh.msroka.exceptions;

public class CompanyInactiveException extends RuntimeException {
    public CompanyInactiveException() {
        super();
    }
}
