package pl.edu.agh.msroka.interceptors;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.edu.agh.msroka.dto.ValidationFailDTO;
import pl.edu.agh.msroka.exceptions.AuthException;
import pl.edu.agh.msroka.exceptions.CompanyInactiveException;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class AppExceptionHandler {
    private class ErrorStatus {
        String status;

        public ErrorStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(CompanyInactiveException.class)
    @ResponseBody
    public ErrorStatus handleCompanyInactiveException(CompanyInactiveException e) {
        return new ErrorStatus("COMPANY_INACTIVE");
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(AuthException.class)
    @ResponseBody
    public String handleAuthException(AuthException e) {
        return e.getMessage();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public List<ValidationFailDTO> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        List<ValidationFailDTO> validationFailDTOs = new ArrayList<>();
        for(FieldError fieldError : fieldErrors) {
            ValidationFailDTO validationFailDTO = new ValidationFailDTO();
            validationFailDTO.setField(fieldError.getField());
            validationFailDTO.setMessage(fieldError.getDefaultMessage());
            validationFailDTOs.add(validationFailDTO);
        }
        return validationFailDTOs;

    }
}
