package pl.edu.agh.msroka.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pl.edu.agh.msroka.daos.CompanyDAO;
import pl.edu.agh.msroka.entities.Company;
import pl.edu.agh.msroka.exceptions.AuthException;
import pl.edu.agh.msroka.exceptions.CompanyInactiveException;
import pl.edu.agh.msroka.services.CompanyService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    CompanyService companyService;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        // check if controller is restriced by annotations
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        boolean isRestrictedMethod = handlerMethod.getMethod().isAnnotationPresent(TokenAuthorizedOnly.class);
        boolean isRestrictedClass = handlerMethod.getMethod().getDeclaringClass().isAnnotationPresent(TokenAuthorizedOnly.class);
        if(!isRestrictedMethod && !isRestrictedClass) {
            return true;
        }
        boolean validateInactive = true;
        if(isRestrictedClass) {
            TokenAuthorizedOnly tokenAuthorizedOnlyAnnotation = handlerMethod.getMethod().getDeclaringClass().getAnnotation(TokenAuthorizedOnly.class);
            validateInactive = tokenAuthorizedOnlyAnnotation.validateInactive();
        } else if(isRestrictedMethod) {
            TokenAuthorizedOnly tokenAuthorizedOnlyAnnotation = ((HandlerMethod) handler).getMethodAnnotation(TokenAuthorizedOnly.class);
            validateInactive = tokenAuthorizedOnlyAnnotation.validateInactive();
        }


        String userIdString = request.getHeader("X-UserId");

        if(userIdString == null) {
            throw new AuthException("No X-UserId header");
        }
        int userId = Integer.parseInt(userIdString);
        Company company = companyService.findCompanyByUserId(userId);
        if (company == null) {
            companyService.createEmptyCompany(userId);
        } else if (!company.isActive() && validateInactive) {
            throw new CompanyInactiveException();
        }
        return super.preHandle(request, response, handler);
    }
}