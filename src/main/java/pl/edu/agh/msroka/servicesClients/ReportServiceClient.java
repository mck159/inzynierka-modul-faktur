package pl.edu.agh.msroka.servicesClients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.msroka.servicesClients.dto.AddReportDTO;

@Component
public class ReportServiceClient {
    @Autowired
    RestTemplate restTemplate;

    @Value("${services.urls.reporting}")
    String url;

    public String addReport(AddReportDTO addReportDTO) {
        return restTemplate.postForObject(url + "/report", addReportDTO, String.class);
    }

    public AddReportDTO getReport(String refReportId) {
        return restTemplate.getForObject(url + "/report?id=" + refReportId, AddReportDTO.class);
    }

    public void deleteReport(String refReportId) {
        restTemplate.delete(url + "/report?id=" + refReportId   );
    }
}
