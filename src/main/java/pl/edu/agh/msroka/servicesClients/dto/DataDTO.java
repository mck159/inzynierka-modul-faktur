package pl.edu.agh.msroka.servicesClients.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by maciek on 29.11.15.
 */
public class DataDTO {
    List<String> recipients;
    Map<String,Object> data;

    public DataDTO() {
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
