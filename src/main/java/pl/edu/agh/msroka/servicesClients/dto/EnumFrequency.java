package pl.edu.agh.msroka.servicesClients.dto;

/**
 * Created by maciek on 26.11.15.
 */
public enum EnumFrequency {
    DAILY, WEEKLY, MONTHLY
}
