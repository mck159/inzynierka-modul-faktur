package pl.edu.agh.msroka.daos;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.edu.agh.msroka.daos.utils.GenericCRUDDao;
import pl.edu.agh.msroka.entities.Company;
import pl.edu.agh.msroka.entities.Invoice;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Repository
public class InvoiceDAO extends GenericCRUDDao<Invoice, Integer> {
    public InvoiceDAO() {
        super(Invoice.class);
    }

    public List<Invoice> findByCompanyAndClientNip(Company company, String clientNip) {
        Session currentSession = this.getCurrentSession();
        Criteria criteria = currentSession .createCriteria(Invoice.class);
        criteria.createAlias("client", "c");
        criteria.add(Restrictions.eq("company", company));
        criteria.add(Restrictions.eq("c.nip", clientNip));
        return criteria.list();
    }


    public HashMap<String, String> getCurrentYearAmountAggregatedByCompany(Company company) {
        Session currentSession = this.getCurrentSession();
        Query query = currentSession.createQuery("SELECT new Map(sum(inv.amount) as amount_sum, avg(inv.amount) as amount_avg, sum(inv.paidAmount) as paid_sum, avg(inv.paidAmount) as paid_avg) FROM Invoice inv WHERE year(inv.createdAt) = year(current_date()) AND inv.company = :company");
        query.setParameter("company", company);
        return (HashMap<String, String>) query.uniqueResult();
    }

    public HashMap<String, Object> getInvoiceSummaryReport(Company company, String aggregation) {
        Session currentSession = this.getCurrentSession();
        String dateFunction = "";
        switch(aggregation) {
            case "DAILY":
                dateFunction = "day";
            case "WEEKLY":
                dateFunction = "week";
            case "MONTHLY":
                dateFunction = "month";
        }
        Query query = currentSession.createQuery(String.format("SELECT new Map(sum(inv.amount) as amount_sum, avg(inv.amount) as amount_avg, sum(inv.paidAmount) as paid_sum, avg(inv.paidAmount) as paid_avg) FROM Invoice inv WHERE %s(inv.createdAt) = %s(current_date()) AND inv.company = :company", dateFunction, dateFunction));
        query.setParameter("company", company);
        return (HashMap<String, Object>) query.uniqueResult();
    }

    public List<Invoice> getOverdueInvoices(Company company, int overdueDays) {
        Session currentSession = this.getCurrentSession();
        Criteria criteria = currentSession .createCriteria(Invoice.class);
        criteria.add(Restrictions.eq("company", company));
        criteria.add(Restrictions.le("payoffDate", getTodayDateShifted(-overdueDays)));
        return criteria.list();
    }

    private Date getTodayDateShifted(int daysShift) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, daysShift);
        return cal.getTime();
    }
}
