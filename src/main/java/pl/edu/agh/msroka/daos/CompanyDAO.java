package pl.edu.agh.msroka.daos;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.edu.agh.msroka.daos.utils.GenericCRUDDao;
import pl.edu.agh.msroka.entities.Company;
import pl.edu.agh.msroka.entities.Invoice;

@Repository
public class CompanyDAO extends GenericCRUDDao<Company, Integer> {
    public CompanyDAO() {
        super(Company.class);
    }

    public Company findByUserId(int userId) {
        Session currentSession = this.getCurrentSession();
        Criteria criteria = currentSession.createCriteria(Company.class);
        criteria.add(Restrictions.eq("refUserId", userId));
        return (Company) criteria.uniqueResult();
    }
}
