package pl.edu.agh.msroka.daos.utils;

import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

public abstract class GenericCRUDDao<T, Id extends Serializable>{
    @Autowired
    SessionFactory sf;
    @PersistenceContext
    EntityManager em;

    protected Session getCurrentSession() {
        return em.unwrap(Session.class);
    }
    protected EntityManager getEntityManager() { return em; }


    private Class<T> type;
    public GenericCRUDDao(Class<T> type) {
        this.type = type;
    }

    public int create(T obj) {
        Session currentSession = getCurrentSession();
        EntityManager em = getEntityManager();
        em.persist(obj);
//        Id id = (Id) currentSession.save(obj);
//        currentSession.flush();
        return 0;
    }

    public T read(Id id) {
        Session currentSession = getCurrentSession();
        T result = (T) currentSession.get(type, id);
        return result;
    }

    public void update(T obj) {
        Session currentSession = getCurrentSession();
        currentSession.update(obj);
    }

    public void delete(Id id) {
        getEntityManager().remove(read(id));
    }

//    public T findOneByField(String fieldName, Object fieldValue) {
//        Session currentSession = getCurrentSession();
//        Criteria criteria = currentSession .createCriteria(this.type);
//        return (T) criteria.add(Restrictions.eq(fieldName, fieldValue)).uniqueResult();
//    }
}
