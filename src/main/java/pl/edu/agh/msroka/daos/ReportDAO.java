package pl.edu.agh.msroka.daos;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.edu.agh.msroka.daos.utils.GenericCRUDDao;
import pl.edu.agh.msroka.entities.Company;
import pl.edu.agh.msroka.entities.Report;
import pl.edu.agh.msroka.entities.ReportType;

@Repository
public class ReportDAO extends GenericCRUDDao<Report, Integer> {
    public ReportDAO() {
        super(Report.class);
    }

    public Report findByType(Company company, ReportType type) {
        Session currentSession = this.getCurrentSession();
        Criteria criteria = currentSession .createCriteria(Report.class);
        criteria.add(Restrictions.eq("company", company));
        criteria.add(Restrictions.eq("type", type));
        return (Report) criteria.uniqueResult();
    }
}
