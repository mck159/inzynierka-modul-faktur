package pl.edu.agh.msroka.daos;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import pl.edu.agh.msroka.daos.utils.GenericCRUDDao;
import pl.edu.agh.msroka.entities.Client;
import pl.edu.agh.msroka.entities.Company;

import java.util.List;

@Repository
public class ClientDAO extends GenericCRUDDao<Client, Integer> {
    public ClientDAO() {
        super(Client.class);
    }

    public List<Client> getNewestClientsWithDistinctNIPsByCompany(Company company) {
        Session session = getCurrentSession();
        int company_id = company.getId();
        String queryString = String.format("SELECT c.* FROM clients c INNER JOIN ( SELECT MAX(updated_at) updated_at, nip FROM clients WHERE company_id='%d' GROUP BY nip) s ON s.nip = c.nip AND s.updated_at = c.updated_at", company_id);
        SQLQuery query = session.createSQLQuery(queryString).addEntity(Client.class);
        return query.list();
    }
}
