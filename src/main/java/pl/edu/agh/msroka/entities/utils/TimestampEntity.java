package pl.edu.agh.msroka.entities.utils;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class TimestampEntity {
    @Temporal(TemporalType.TIMESTAMP)
    Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    Date updatedAt;

    @PrePersist
    protected void onCreate() {
        this.createdAt = this.updatedAt = new Date();
    }

    @PreUpdate
    void onUpdate() {
        this.updatedAt = new Date();
    }
}
