package pl.edu.agh.msroka.entities.utils;

public enum PaymentForm {
    TRANSFER, CARD, CASH
}
