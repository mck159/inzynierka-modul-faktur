package pl.edu.agh.msroka.entities;

/**
 * Created by maciek on 29.11.15.
 */
public enum ReportType {
    SUMMARY, REMIND_OVERDUE
}
