package pl.edu.agh.msroka.entities;

import pl.edu.agh.msroka.entities.utils.PaymentForm;
import pl.edu.agh.msroka.entities.utils.TimestampEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.Random;
import java.util.Set;

@Entity
@Table(name="invoices")
public class Invoice extends TimestampEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Integer id;

    String number;

    Date issueDate;
    String issueCity;

    Date saleDate;

    Date payoffDate;

    Double amount;
    Double paidAmount;

    @Enumerated(EnumType.STRING)
    PaymentForm paymentForm;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval=true)
    Set<Item> items;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval=true)
    Set<Payment> payments;

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "client_id")
    Client client;

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "seller_id")
    Seller seller;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "company_id")
    Company company;

    public Invoice() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueCity() {
        return issueCity;
    }

    public void setIssueCity(String issueCity) {
        this.issueCity = issueCity;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public Date getPayoffDate() {
        return payoffDate;
    }

    public void setPayoffDate(Date payoffDate) {
        this.payoffDate = payoffDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public PaymentForm getPaymentForm() {
        return paymentForm;
    }

    public void setPaymentForm(PaymentForm paymentForm) {
        this.paymentForm = paymentForm;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<Payment> getPayments() {
        return payments;
    }

    public void setPayments(Set<Payment> payments) {
        this.payments = payments;
    }

    @PrePersist
    @PreUpdate
    void updateAmount() {
        double sum = 0;
        Set<Item> items = this.getItems();
        if(items == null) {
            return;
        }
        for(Item item : items) {
            sum += item.getPriceNet()*item.getQuantity()*(1 + (item.getVat()/100.0));
        }
        this.setAmount(sum);

        sum = 0;
        Set<Payment> payments = this.getPayments();
        if(payments == null) {
            return;
        }
        for(Payment payment : payments) {
            sum += payment.getAmount();
        }
        this.setPaidAmount(sum);
    }
}
