package pl.edu.agh.msroka.entities;

import pl.edu.agh.msroka.entities.utils.TimestampEntity;

import javax.persistence.*;

@Entity
@Table(name="payments")
public class Payment extends TimestampEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    int id;
    double amount;
    String description;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_id")
    Invoice invoice;

    public Payment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
