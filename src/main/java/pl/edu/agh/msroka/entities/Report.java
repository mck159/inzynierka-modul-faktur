package pl.edu.agh.msroka.entities;

import pl.edu.agh.msroka.entities.utils.TimestampEntity;

import javax.persistence.*;

@Entity
@Table(name="reports")
public class Report extends TimestampEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    int id;
    ReportType type;
    String refReportId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    Company company;

    public Report() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ReportType getType() {
        return type;
    }

    public void setType(ReportType type) {
        this.type = type;
    }

    public String getRefReportId() {
        return refReportId;
    }

    public void setRefReportId(String refReportId) {
        this.refReportId = refReportId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
