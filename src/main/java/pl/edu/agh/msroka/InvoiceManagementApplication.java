package pl.edu.agh.msroka;

import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.msroka.daos.InvoiceDAO;

import javax.persistence.EntityManagerFactory;

@SpringBootApplication
public class InvoiceManagementApplication {
    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Bean
    InvoiceDAO invoiceDAO() {
        return new InvoiceDAO();
    }

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
    @Bean
    public SessionFactory sessionFactory(HibernateEntityManagerFactory hemf) {
        return hemf.getSessionFactory();
    }

    public static void main(String[] args) {
        SpringApplication.run(InvoiceManagementApplication.class, args);
    }
}
