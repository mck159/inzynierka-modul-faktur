package pl.edu.agh.msroka.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.edu.agh.msroka.dto.ClientDTO;
import pl.edu.agh.msroka.interceptors.TokenAuthorizedOnly;
import pl.edu.agh.msroka.services.ClientService;

import java.util.Set;

@Controller
@RequestMapping("/client")
@TokenAuthorizedOnly
public class ClientController {
    @Autowired
    ClientService clientService;

    @RequestMapping(value="", method = RequestMethod.GET)
    @ResponseBody
    Set<ClientDTO> getNewestClientsWithDistinctNIPs(@RequestHeader("X-UserId") int userId) {
        return clientService.getNewestClientsWithDistinctNIPsByUserId(userId);
    }
}
