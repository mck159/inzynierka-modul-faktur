package pl.edu.agh.msroka.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.msroka.dto.CompanyDTO;
import pl.edu.agh.msroka.dto.InvoiceDTO;
import pl.edu.agh.msroka.interceptors.TokenAuthorizedOnly;
import pl.edu.agh.msroka.services.CompanyService;

import javax.validation.Valid;

@Controller
@RequestMapping("/company")
@TokenAuthorizedOnly(validateInactive = false)
public class CompanyController {
    @Autowired
    CompanyService companyService;

    @RequestMapping(value="", method = RequestMethod.GET)
    @ResponseBody
    CompanyDTO getCompany(@RequestHeader("X-UserId") int userId) {
        return companyService.findByUserId(userId);
    }

    @RequestMapping(value="", method = RequestMethod.PUT)
    @ResponseBody
    Integer updateCompany(@RequestHeader("X-UserId") int userId, @Valid @RequestBody CompanyDTO companyDTO) {
        return companyService.updateCompany(userId, companyDTO);
    }
}
