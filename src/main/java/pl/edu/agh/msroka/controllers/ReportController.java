package pl.edu.agh.msroka.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.msroka.dto.ReportDTO;
import pl.edu.agh.msroka.entities.ReportType;
import pl.edu.agh.msroka.interceptors.TokenAuthorizedOnly;
import pl.edu.agh.msroka.services.InvoiceService;
import pl.edu.agh.msroka.services.ReportService;
import pl.edu.agh.msroka.servicesClients.dto.DataDTO;

import java.util.List;

@RestController
@RequestMapping("/invoice/report")
@TokenAuthorizedOnly
public class ReportController {
    @Autowired
    ReportService reportService;

    @Autowired
    InvoiceService invoiceService;

    @RequestMapping(value="", method = RequestMethod.POST)
    void addReport(@RequestHeader("X-UserId") int userId, @RequestBody ReportDTO reportDTO) {
        reportService.addReport(reportDTO, userId);
    }
    @RequestMapping(value="", method = RequestMethod.DELETE)
    void addReport(@RequestHeader("X-UserId") int userId, @RequestParam("type") ReportType reportType) {
        reportService.removeReportByUserAndType(userId, reportType);
    }
    @RequestMapping(value="", method = RequestMethod.GET)
    List<ReportDTO> getReports(@RequestHeader("X-UserId") int userId) {
        return reportService.getReportsByUser(userId);
    }

    @RequestMapping(value="/summaryReport", method = RequestMethod.GET)
    @ResponseBody
    DataDTO getSummaryReport(@RequestParam("company_id") int company_id, @RequestParam("aggregation") String aggregation) {
        return invoiceService.getInvoiceSummaryReport(company_id, aggregation);
    }

    @RequestMapping(value="/remindOverdueReports", method = RequestMethod.GET)
    @ResponseBody
    List<DataDTO> getRemindOverdueReports(@RequestParam("company_id") int company_id, @RequestParam("overdue_days") int overdue_days) {
        return invoiceService.getRemindOverdueReports(company_id, overdue_days);
    }
}