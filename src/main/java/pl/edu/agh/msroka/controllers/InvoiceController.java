package pl.edu.agh.msroka.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.msroka.dto.DTOConverter;
import pl.edu.agh.msroka.dto.InvoiceDTO;
import pl.edu.agh.msroka.entities.Invoice;
import pl.edu.agh.msroka.exceptions.ResourceNotFoundException;
import pl.edu.agh.msroka.interceptors.TokenAuthorizedOnly;
import pl.edu.agh.msroka.services.InvoiceService;

import javax.validation.Valid;
import java.util.Set;

@Controller
@RequestMapping("/invoice")
@TokenAuthorizedOnly
public class InvoiceController {
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    DTOConverter dtoConverter;


    @RequestMapping(value="/{invoice_id}", method = RequestMethod.GET)
    @ResponseBody
    InvoiceDTO getInvoice(@RequestHeader("X-UserId") int userId, @PathVariable int invoice_id) {
        Invoice invoice = invoiceService.find(invoice_id, userId);
        if(invoice == null) {
            throw new ResourceNotFoundException();
        }
        return dtoConverter.toInvoiceDTO(invoice);
    }

    @RequestMapping(value="", method = RequestMethod.GET)
    @ResponseBody
    Set<InvoiceDTO> getInvoices(@RequestHeader("X-UserId") int userId, @RequestParam(value="client_nip", required=false) String clientNip) {
        return clientNip == null ? invoiceService.findByUserId(userId) : invoiceService.findByUserIdAndClientNip(userId, clientNip);
    }

    @RequestMapping(value="", method = RequestMethod.POST)
    @ResponseBody
    Integer addInvoice(@RequestHeader("X-UserId") int userId, @Valid @RequestBody InvoiceDTO invoiceDTO) {
        return invoiceService.add(invoiceDTO, userId);
    }

    @RequestMapping(value="/{invoice_id}", method = RequestMethod.DELETE)
    @ResponseBody
    void deleteInvoice(@RequestHeader("X-UserId") int userId, @PathVariable int invoice_id) {
        invoiceService.delete(invoice_id, userId);
    }

    @RequestMapping(value="/{invoice_id}", method = RequestMethod.PUT)
    @ResponseBody
    void updateInvoice(@RequestHeader("X-UserId") int userId, @PathVariable int invoice_id, @Valid @RequestBody InvoiceDTO invoiceDTO) {
        invoiceService.update(invoice_id, invoiceDTO, userId);
    }
}
