package pl.edu.agh.msroka.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.msroka.daos.ClientDAO;
import pl.edu.agh.msroka.daos.CompanyDAO;
import pl.edu.agh.msroka.dto.ClientDTO;
import pl.edu.agh.msroka.dto.DTOConverter;
import pl.edu.agh.msroka.entities.Client;
import pl.edu.agh.msroka.entities.Company;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ClientService {
    @Autowired
    ClientDAO clientDAO;

    @Autowired
    CompanyDAO companyDAO;

    @Autowired
    DTOConverter dtoConverter;


    public Set<ClientDTO> getNewestClientsWithDistinctNIPsByUserId(int userId) {
        Company company = companyDAO.findByUserId(userId);
        List<Client> clients = clientDAO.getNewestClientsWithDistinctNIPsByCompany(company);
        Set<ClientDTO> clientDTOs = new HashSet<>();
        if(clients != null) {
            for(Client client : clients) {
                clientDTOs.add(dtoConverter.toClientDTO(client));
            }
        }
        return clientDTOs;
    }
}
