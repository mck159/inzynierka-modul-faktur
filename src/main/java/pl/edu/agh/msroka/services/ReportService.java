package pl.edu.agh.msroka.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.msroka.daos.CompanyDAO;
import pl.edu.agh.msroka.daos.InvoiceDAO;
import pl.edu.agh.msroka.daos.ReportDAO;
import pl.edu.agh.msroka.dto.CompanyDTO;
import pl.edu.agh.msroka.dto.DTOConverter;
import pl.edu.agh.msroka.dto.InvoiceDTO;
import pl.edu.agh.msroka.dto.ReportDTO;
import pl.edu.agh.msroka.entities.Company;
import pl.edu.agh.msroka.entities.Invoice;
import pl.edu.agh.msroka.entities.Report;
import pl.edu.agh.msroka.entities.ReportType;
import pl.edu.agh.msroka.exceptions.AuthException;
import pl.edu.agh.msroka.exceptions.ResourceAlreadyExistsException;
import pl.edu.agh.msroka.exceptions.ResourceNotFoundException;
import pl.edu.agh.msroka.servicesClients.ReportServiceClient;
import pl.edu.agh.msroka.servicesClients.dto.AddReportDTO;
import pl.edu.agh.msroka.servicesClients.dto.EnumFrequency;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Service
@Transactional
public class ReportService {
    @Autowired
    CompanyService companyService;
    @PersistenceContext
    EntityManager em;
    @Autowired
    DTOConverter dtoConverter;
    @Autowired
    ReportServiceClient reportServiceClient;
    @Autowired
    ReportDAO reportDAO;


    public int addReport(ReportDTO reportDTO, int userId) {
        if(checkIfExistsByUserAndType(userId, reportDTO.getType())) {
            throw new ResourceAlreadyExistsException();
        }
        Report report = new Report();
        report.setCompany(companyService.findCompanyByUserId(userId));
        report.setType(reportDTO.getType());

        Company company = companyService.findCompanyByUserId(userId);
        AddReportDTO addReportDTO = new AddReportDTO();
        addReportDTO.setFrequency(EnumFrequency.valueOf((String) reportDTO.getParams().get("frequency")));
        addReportDTO.setServiceName("invoices");
        if(reportDTO.getType().equals(ReportType.SUMMARY)) {
            addReportDTO.setReportName("invoices_summary");
            addReportDTO.setResourceUrl("invoice/report/summaryReport?aggregation={aggregation}&company_id={company_id}");
            Map<String, Object> params = new HashMap<>();
            params.put("aggregation", reportDTO.getParams().get("frequency"));
            params.put("company_id", company.getId());
            addReportDTO.setResourceParams(params);
            addReportDTO.setSubject("Invoices summary " + reportDTO.getParams().get("frequency"));
            addReportDTO.setMulti(false);
        } else if(reportDTO.getType().equals(ReportType.REMIND_OVERDUE)) {
            addReportDTO.setReportName("invoices_remind_overdue");
            addReportDTO.setResourceUrl("invoice/report/remindOverdueReports?company_id={company_id}&overdue_days={overdue_days}");
            Map<String, Object> params = new HashMap<>();
            params.put("company_id", company.getId());
            params.put("overdue_days", reportDTO.getParams().get("overdue_days"));
            addReportDTO.setResourceParams(params);
            addReportDTO.setSubject("Your overdue invoices");
            addReportDTO.setMulti(true);
        }
        String refId = reportServiceClient.addReport(addReportDTO);
        report.setRefReportId(refId);
        em.persist(report);
        return report.getId();
    }

    public boolean checkIfExistsByUserAndType(int userId, ReportType reportType) {
        Company company = companyService.findCompanyByUserId(userId);
        Report report = reportDAO.findByType(company, reportType);
        return report != null;
    }

    public void removeReportByUserAndType(int userId, ReportType reportType) {
        Company company = companyService.findCompanyByUserId(userId);
        Report report = reportDAO.findByType(company, reportType);
        if(report == null) {
            throw new ResourceNotFoundException();
        }
        reportServiceClient.deleteReport(report.getRefReportId());
        em.remove(report);
    }

    public List<ReportDTO> getReportsByUser(int userId) {
        Company company = companyService.findCompanyByUserId(userId);
        List<ReportDTO> reportDTOs = new ArrayList<>();
        for(Report report : company.getReports()) {
            AddReportDTO addReportDTO = reportServiceClient.getReport(report.getRefReportId());
            ReportDTO reportDTO = new ReportDTO();
            Map<String, Object> params = (Map<String, Object>) addReportDTO.getResourceParams();
            params.put("frequency", addReportDTO.getFrequency().toString());
            reportDTO.setParams(params);
            reportDTO.setType(addReportDTO.getReportName().equals("invoices_remind_overdue") ? ReportType.REMIND_OVERDUE : ReportType.SUMMARY);
            reportDTOs.add(reportDTO);
        }
        return reportDTOs;
    }
}
