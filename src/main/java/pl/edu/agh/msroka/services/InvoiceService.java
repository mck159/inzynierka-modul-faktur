package pl.edu.agh.msroka.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.msroka.daos.CompanyDAO;
import pl.edu.agh.msroka.daos.InvoiceDAO;
import pl.edu.agh.msroka.dto.DTOConverter;
import pl.edu.agh.msroka.dto.InvoiceDTO;
import pl.edu.agh.msroka.dto.ReportInvoiceDTO;
import pl.edu.agh.msroka.entities.Company;
import pl.edu.agh.msroka.entities.Invoice;
import pl.edu.agh.msroka.exceptions.AuthException;
import pl.edu.agh.msroka.exceptions.ResourceNotFoundException;
import pl.edu.agh.msroka.servicesClients.dto.DataDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

@Service
@Transactional
public class InvoiceService {
    @Autowired
    InvoiceDAO invoiceDAO;
    @Autowired
    CompanyDAO companyDAO;
    @PersistenceContext
    EntityManager em;
    @Autowired
    DTOConverter dtoConverter;

    public void validateInvoiceCompany(Company company, Invoice invoice, boolean isPersistentInvoice) {
        Company invoiceCompany;
        if(invoice == null) {
            throw new ResourceNotFoundException();
        }
        if(!isPersistentInvoice) {
            Invoice persistentInvoice = invoiceDAO.read(invoice.getId());
            if(persistentInvoice == null) {
                throw new ResourceNotFoundException();
            }
            invoiceCompany = persistentInvoice.getCompany();
        } else {
            invoiceCompany = invoice.getCompany();
        }
        if(!invoiceCompany.equals(company)) {
            throw new AuthException("Restricted");
        }
    }

    public Invoice find(int id, int userId) {
        Company company = companyDAO.findByUserId(userId);
        Invoice invoice = invoiceDAO.read(id);
        validateInvoiceCompany(company, invoice, true);
        return invoice;
    }
    public Integer save(Invoice invoice, int companyId) {
        return invoiceDAO.create(invoice);
    }

    public Set<InvoiceDTO> findByUserId(int userId) {
        Company company = companyDAO.findByUserId(userId);
        Set<InvoiceDTO> invoiceDTOs = new HashSet<>();
        Set<Invoice> invoices = company.getInvoices();
        if(invoices != null) {
            for(Invoice invoice : invoices) {
                invoiceDTOs.add(dtoConverter.toInvoiceDTO(invoice));
            }
        }
        return invoiceDTOs;
    }

    public Integer add(InvoiceDTO invoiceDTO, int userId) {
        Company company = companyDAO.findByUserId(userId);
        Invoice invoice = dtoConverter.fromInvoiceDTO(invoiceDTO);
        invoice.setId(null);
        company.addInvoice(invoice);
        invoice.setCompany(company);
        invoice.getClient().setCompany(company);
        invoice.getSeller().setCompany(company);
        em.persist(invoice);
        return invoice.getId();
    }

    public Integer update(int invoiceId, InvoiceDTO invoiceDTO, int userId) {
        Company company = companyDAO.findByUserId(userId);
        company = em.merge(company);
        Invoice invoice = dtoConverter.fromInvoiceDTO(invoiceDTO);
        invoice.setId(invoiceId);
        validateInvoiceCompany(company, invoice, false);
        company.addInvoice(invoice);
        invoice.setCompany(company);
        invoice.getClient().setCompany(company);
        invoice.getSeller().setCompany(company);
        em.merge(invoice);
        return 0;
    }

    public void delete(Integer invoice_id, int userId) {
        Company company = companyDAO.findByUserId(userId);
        Invoice invoice = invoiceDAO.read(invoice_id);
        if(invoice == null) {
            throw new ResourceNotFoundException();
        }
        validateInvoiceCompany(company, invoice, true);
        em.remove(invoice);
    }

    public Set<InvoiceDTO> findByUserIdAndClientNip(int userId, String clientNip) {
        Company company = companyDAO.findByUserId(userId);
        List<Invoice> invoices = invoiceDAO.findByCompanyAndClientNip(company, clientNip);
        Set<InvoiceDTO> invoiceDTOs = new HashSet<>();

        if(invoices != null) {
            for (Invoice invoice :invoices) {
                invoiceDTOs.add(dtoConverter.toInvoiceDTO(invoice));
            }
        }
        return invoiceDTOs;
    }

    public HashMap<String, String> getCurrentYearAmountAggregatedByCompanyId(int companyId) {
        Company company = companyDAO.read(companyId);
        return invoiceDAO.getCurrentYearAmountAggregatedByCompany(company);
    }

    public DataDTO getInvoiceSummaryReport(int company_id, String aggregation) {
        Company company = companyDAO.read(company_id);
        HashMap<String, Object> data = invoiceDAO.getInvoiceSummaryReport(company, aggregation);
        DataDTO dataDTO = new DataDTO();
        List<String> recipients = new ArrayList<>();
        recipients.add(company.getEmail());
        dataDTO.setRecipients(recipients);
        dataDTO.setData(data);
        return dataDTO;
    }

    public List<DataDTO> getRemindOverdueReports(int company_id, int overdueDays) {
        Company company = companyDAO.read(company_id);
        List<Invoice> overdueInvoices = invoiceDAO.getOverdueInvoices(company, overdueDays);
        List<DataDTO> result = new ArrayList<>();
        Map<String, List<Invoice>> invoicesByClientsMails = new HashMap<>();
        for(Invoice overdueInvoice : overdueInvoices) {
            String clientMail = overdueInvoice.getClient().getEmail();
            List<Invoice> invoicesList = invoicesByClientsMails.get(clientMail);
            if(invoicesList == null) {
                invoicesByClientsMails.put(clientMail, new ArrayList<>());
                invoicesList = invoicesByClientsMails.get(clientMail);
            }
            invoicesList.add(overdueInvoice);
        }
        for(Map.Entry<String, List<Invoice>> invoiceByClientsMail : invoicesByClientsMails.entrySet()) {
            DataDTO dataDTO = new DataDTO();
            dataDTO.setRecipients(Collections.singletonList(invoiceByClientsMail.getKey()));
            Map<String, Object> params = new HashMap<>();
            List<ReportInvoiceDTO> paramsInvoices = new ArrayList<>();
            for(Invoice invoice : invoiceByClientsMail.getValue()) {
                paramsInvoices.add(dtoConverter.toReportInvoiceDTO(invoice));
            }
            params.put("invoices", paramsInvoices);
            dataDTO.setData(params);
            result.add(dataDTO);
        }

        return result;
    }
}
