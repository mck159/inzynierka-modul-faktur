package pl.edu.agh.msroka.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.agh.msroka.daos.ClientDAO;
import pl.edu.agh.msroka.daos.CompanyDAO;
import pl.edu.agh.msroka.dto.ClientDTO;
import pl.edu.agh.msroka.dto.CompanyDTO;
import pl.edu.agh.msroka.dto.DTOConverter;
import pl.edu.agh.msroka.entities.Client;
import pl.edu.agh.msroka.entities.Company;
import pl.edu.agh.msroka.exceptions.ResourceAlreadyExistsException;
import pl.edu.agh.msroka.exceptions.ResourceNotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class CompanyService {
    @Autowired
    CompanyDAO companyDAO;

    @Autowired
    DTOConverter dtoConverter;

    @PersistenceContext
    EntityManager em;


    public CompanyDTO findByUserId(int userId) {
        return dtoConverter.toCompanyDTO(companyDAO.findByUserId(userId));
    }

    public Company find(int companyId) {
        return companyDAO.read(companyId);
    }

    public void createEmptyCompany(int userId) {
        Company company = new Company();
        company.setRefUserId(userId);
        em.persist(company);
    }

    public Integer setCompany(int userId, CompanyDTO companyDTO) {
        Company company = dtoConverter.fromCompanyDTO(companyDTO);
        if(companyDAO.findByUserId(userId) != null) {
            throw new ResourceAlreadyExistsException();
        }
        company.setRefUserId(userId);
        em.persist(company);
        return 0;
    }

    public Integer updateCompany(int userId, CompanyDTO companyDTO) {
        Company dbCompany = companyDAO.findByUserId(userId);
        if(dbCompany == null) {
            throw new ResourceNotFoundException();
        }
        dtoConverter.fromCompanyDTO(companyDTO, dbCompany);
        dbCompany.setActive(true);
        em.merge(dbCompany);
        return 0;
    }

    public Company findCompanyByUserId(int userId) {
        return companyDAO.findByUserId(userId);
    }
}
